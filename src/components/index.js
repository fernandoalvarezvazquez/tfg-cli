import Home from "./Home";
import LoginWeb from "./LoginWeb";
import LoginClub from "./LoginClub";
import NotFound from "./NotFound";
import InicioWeb from "./InicioWeb";
import Reservas from "./Reservas";

export { Home, LoginWeb, LoginClub, NotFound, InicioWeb, Reservas};
