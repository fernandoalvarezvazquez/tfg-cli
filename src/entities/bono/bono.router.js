import BonoList from "./BonoList";
import BonoForm from "./BonoForm";

const routes = [
    {
        name: "BonoList",
        path: "/bonos",
        component: BonoList,
        meta: { public: false, authority: 'ADMIN_CLUB'},
    },
    {
        name: "BonoCreate",
        path: "/bono/new",
        component: BonoForm
    },
    {
        name: "BonoUpdate",
        path: "/bonos/:id/update",
        component: BonoForm,
    },
];

export default routes;