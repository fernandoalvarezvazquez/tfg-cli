import TemaList from "./TemaList";
import TemaForm from "./TemaForm";
import TemaDetail from "./TemaDetail";

const routes = [
  {
    name: "TemaList",
    path: "/temas",
    component: TemaList,
    meta: { public: true },
  },
  // /posts/new debe colocarse antes de /posts/:id porque si no vue-router
  // interpreta "new" como si fuera el id.
  //
  // Una forma de evitar este problema es usar una expresión regular para
  // limitar los valores que son interpretados. Por ejemplo, usando el path
  // /posts/:id(\\d+), vue-router espera que :id sea numérico.
  {
    name: "TemaCreate",
    path: "/temas/new",
    component: TemaForm,
  },
  {
    name: "TemaDetail",
    path: "/temas/:id",
    component: TemaDetail,
    meta: { public: true },
  },
  {
    name: "TemaUpdate",
    path: "/temas/:id/update",
    component: TemaForm,
  },
];

export default routes;
