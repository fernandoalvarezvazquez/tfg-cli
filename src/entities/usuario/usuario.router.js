import UsuarioList from "./UsuarioList";
import UsuarioForm from "./UsuarioForm";
import UsuarioContrasenaForm from "./UsuarioContrasenaForm";
import UsuarioDetail from "./UsuarioDetail";
import UsuarioDetailPerfil from "./UsuarioDetailPerfil";

const routes = [
  {
    name: "UsuarioList",
    path: "/usuarios",
    component: UsuarioList,
    meta: { public: true },
  },
  // /posts/new debe colocarse antes de /posts/:id porque si no vue-router
  // interpreta "new" como si fuera el id.
  //
  // Una forma de evitar este problema es usar una expresión regular para
  // limitar los valores que son interpretados. Por ejemplo, usando el path
  // /posts/:id(\\d+), vue-router espera que :id sea numérico.
  {
    name: "UsuarioCreate",
    path: "/register",
    component: UsuarioForm,
  },
  {
    name: "UsuarioDetail",
    path: "/account",
    component: UsuarioDetail,
    meta: { public: true },
  },
  {
    name: "UsuarioDetailPerfil",
    path: "/perfil",
    component: UsuarioDetailPerfil,
  },
  {
    name: "UsuarioUpdate",
    path: "/usuarios/:id/update",
    component: UsuarioForm,
  },
  {
    name: "UsuarioUpdateContraseña",
    path: "/usuarios/:id/update-contra",
    component: UsuarioContrasenaForm,
  },
];

export default routes;
