import ClubList from "./ClubList";
import ClubDetail from "./ClubDetail";
import ClubDetailAC from "./ClubDetailAC";
import ClubForm from "./ClubForm";
import ClubFormAC from "./ClubFormAC";
import UsuarioAdminList from "@/entities/usuario/UsuarioAdminList";

const routes = [
    {
        name: "ClubList",
        path: "/clubes",
        component: ClubList,
        meta: { public: false, authority: 'ADMIN_WEB'},
    },
    {
        name: "ClubCreate",
        path: "/club/new",
        component: ClubForm
    },
    {
      name: "ClubAddAdminList",
      path: "/clubes/:id/addAdmin",
      component: UsuarioAdminList,
      meta: { public: true },
    },
    {
      name: "ClubDetail",
      path: "/club/:id",
      component: ClubDetail,
      meta: { public: false },
    },
    {
      name: "ClubDetailAC",
      path: "/club-admin/:id",
      component: ClubDetailAC,
      meta: { public: false },
    },
    {
      name: "ClubUpdate",
      path: "/clubes/:id/update",
      component: ClubForm,
    },
    {
      name: "ClubUpdateAC",
      path: "/club-admin/:id/update",
      component: ClubFormAC,
    },
];

export default routes;