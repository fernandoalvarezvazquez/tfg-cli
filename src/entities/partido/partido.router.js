import PartidoForm from "./PartidoForm";
import PartidoFormAC from "./PartidoFormAC";

const routes = [
    {
        name: "PartidoCreate",
        path: "/partido/new",
        component: PartidoForm,
    },
    {
        name: "PartidoCreateAC",
        path: "/partido/new-admin",
        component: PartidoFormAC,
    },
    {
        name: "PartidoUpdate",
        path: "/partidos/:id/update",
        component: PartidoForm,
    },

];

export default routes;