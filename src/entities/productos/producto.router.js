import ProductoList from "./ProductoList";
import ProductoForm from "./ProductoForm";
import ProductoValorarForm from "./ProductoValorarForm";
import ProductoDetail from "./ProductoDetail";
import ProductoListTema from "./ProductoListTema";
import ProductoListCompras from "./ProductoListCompras";
import ProductoListMegusta from "./ProductoListMegusta";
import ProductoListVendidos from "./ProductoListVendidos";

const routes = [
  {
    name: "ProductoList",
    path: "/productos",
    component: ProductoList,
    meta: { public: true },
  },
  {
    name: "ProductoListTema",
    path: "/productos/tema",
    component: ProductoListTema,
    meta: { public: true },
  },
  {
    name: "ProductoListCompras",
    path: "/usuario/compra",
    component: ProductoListCompras,
  },
  {
    name: "ProductoListMegusta",
    path: "/usuario/megusta",
    component: ProductoListMegusta,
  },
  {
    name: "ProductoListVendidos",
    path: "/usuario/vendidos",
    component: ProductoListVendidos,
  },
  // /productos/new debe colocarse antes de /productos/:id porque si no vue-router
  // interpreta "new" como si fuera el id.
  //
  // Una forma de evitar este problema es usar una expresión regular para
  // limitar los valores que son interpretados. Por ejemplo, usando el path
  // /posts/:id(\\d+), vue-router espera que :id sea numérico.
  {
    name: "ProductoCreate",
    path: "/productos/new",
    component: ProductoForm
  },
  {
    name: "ProductoValorarForm",
    path: "/productos/:id/valorar",
    component: ProductoValorarForm
  },
  {
    name: "ProductoDetail",
    path: "/productos/:id",
    component: ProductoDetail,
    meta: { public: true },
  },
  {
    name: "ProductoUpdate",
    path: "/productos/:id/update",
    component: ProductoForm,
  },
];

export default routes;