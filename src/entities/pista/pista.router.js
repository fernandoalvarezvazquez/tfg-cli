import PistaForm from "./PistaForm";
import PistaFormAC from "./PistaFormAC";
import PistaList from "./PistaList";

const routes = [
    {
        name: "PistaList",
        path: "/pistas/:idClub",
        component: PistaList,
    },
    {
        name: "PistaCreate",
        path: "/pista/new/:idClub",
        component: PistaForm
    },
    {
        name: "PistaUpdate",
        path: "/pistas/:id/update",
        component: PistaForm,
    },
    {
        name: "PistaUpdateAC",
        path: "/pistas/:id/updateAC",
        component: PistaFormAC,
    },
];

export default routes;