import MensajeList from "./MensajeList";
import ChatList from "./ChatList";

const routes = [
    {
      name: "MensajeList",
      path: "/conversacion",
      component: MensajeList,
      meta: { public: true },
    },
    {
      name: "ChatList",
      path: "/chats",
      component: ChatList,
      meta: { public: true },
    },
    
  ];

export default routes;
