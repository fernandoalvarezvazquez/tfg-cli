import HTTP from "@/common/http";

const resource = "bonos";

export default{
    async findAllByClub(id) {
        return (await HTTP.get(`${resource}/club/${id}`)).data;
    },
    async findOne(id) {
        return (await HTTP.get(`${resource}/${id}`)).data;
    },
    async delete(id) {
        return await HTTP.delete(`${resource}/${id}`);
    },
    async save(bono) {
        if (bono.id) {
          return (await HTTP.put(`${resource}/${bono.id}`, bono)).data;
        } else {
          return (await HTTP.post(`${resource}`, bono)).data;
        }
    },
}