import HTTP from "@/common/http";

const resource = "temas";

// función para hacer las llamadas lentas a propósito
function sleep(ms) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}

export default {
  async findAll() {
    const response = await HTTP.get(resource);
    return response.data;
  },

  async findOne(id) {
    await sleep(1000);
    return (await HTTP.get(`${resource}/${id}`)).data;
  },

  async delete(id) {
    await sleep(1000);
    return await HTTP.delete(`${resource}/${id}`);
  },

  async save(tema){
    if (tema.id) {
      return (await HTTP.put(`${resource}/${tema.id}`, tema)).data;
    } else {
      return (await HTTP.post(`${resource}`, tema)).data;
    }
  }
};
