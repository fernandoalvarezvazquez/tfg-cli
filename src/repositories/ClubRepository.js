import HTTP from "@/common/http";

const resource = "clubes";

export default{
    async findAll() {
        const response = await HTTP.get(resource);
        return response.data;
    },
    async findOne(id) {
        return (await HTTP.get(`${resource}/${id}`)).data;
    },
    async findByAdmin(id){
        return(await HTTP.get(`${resource}/admin?idAdmin=${id}`)).data;
    },
    async delete(id) {
        return await HTTP.delete(`${resource}/${id}`);
    },
    async save(club) {
        if (club.id) {
          return (await HTTP.put(`${resource}/${club.id}`, club)).data;
        } else {
          return (await HTTP.post(`${resource}`, club)).data;
        }
    },

}