import HTTP from "@/common/http";

const resource = "partidos";

export default{

    async findAllByClub(idClub){
        return (await HTTP.get(`${resource}/club/${idClub}`)).data;
    },
    async findOne(id) {
        return (await HTTP.get(`${resource}/${id}`)).data;
    },
    async save(partido) {
        if (partido.id) {
          return (await HTTP.put(`${resource}/${partido.id}`, partido)).data;
        } else {
          return (await HTTP.post(`${resource}`, partido)).data;
        }
    },
    async delete(id) {
      return await HTTP.delete(`${resource}/${id}`);
  },
}