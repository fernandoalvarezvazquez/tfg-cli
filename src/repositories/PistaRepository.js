import HTTP from "@/common/http";

const resource = "pistas";

export default{
    async findAllByClub(idClub) {
        const response = await HTTP.get(`${resource}?id=${idClub}`);
        return response.data;
    },
    async findOne(id) {
        return (await HTTP.get(`${resource}/${id}`)).data;
    },
    async delete(id) {
        return await HTTP.delete(`${resource}/${id}`);
    },
    async save(pista) {
        if (pista.id) {
          return (await HTTP.put(`${resource}/${pista.id}`, pista)).data;
        } else {
          return (await HTTP.post(`${resource}`, pista)).data;
        }
    },
}
