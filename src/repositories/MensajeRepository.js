import HTTP from "@/common/http";

const resource = "mensajes";

// función para hacer las llamadas lentas a propósito
/*function sleep(ms) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}*/

export default {
    async findConversacion(id1, id2) {
      const response = await HTTP.get(`${resource}/conversacion/${id1}?id2=`+id2);
      return response.data;
    },

    async enviarMensaje(mensaje){
      return (await HTTP.post(`${resource}`, mensaje)).data;
    }
  
};