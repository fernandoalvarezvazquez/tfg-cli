import HTTP from "@/common/http";

const resource = "productos";

// función para hacer las llamadas lentas a propósito
function sleep(ms) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}

export default {
  async findAll(query, sort) {
    const params = new URLSearchParams();
    if (query) params.append("query", query);
    if (sort) params.append("sort", sort);
    const paramsStr = params.toString();
    let url = resource;
    if (paramsStr) url += "?" + paramsStr;
    const response = await HTTP.get(url);
    return response.data;
  },

  async findOne(id) {
    return (await HTTP.get(`${resource}/${id}`)).data;
  },

  async save(prod) {
    if (prod.id) {
      return (await HTTP.put(`${resource}/${prod.id}`, prod)).data;
    } else {
      return (await HTTP.post(`${resource}`, prod)).data;
    }
  },

  async updateFechaCompra(prod){
    return (await HTTP.put(`${resource}/${prod.id}/valoracion`, prod)).data;
  },

  async delete(id) {
    return await HTTP.delete(`${resource}/${id}`);
  },

  async saveImage(id, file) {
    const formData = new FormData();
    formData.append("file", file);
    return (
      await HTTP.post(`${resource}/${id}/image`, formData, {
        headers: {
          "Content-Type": "multipart/form-data",
        },
      })
    ).data;
  },

  async findByTema(id) {
    await sleep(1000);
    return (await HTTP.get(`${resource}/tema/${id}`)).data;
  },

  async findByKeyword(name){
    return (await HTTP.get(`${resource}/keyword/titulo?`+name)).data;
  }
};
