import HTTP from "@/common/http";

const resource = "usuarios";

// función para hacer las llamadas lentas a propósito
function sleep(ms) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}

export default {
  async findAll() {
    const response = await HTTP.get(resource);
    return response.data;
  },

  async findAllPlayers(){
    const response = await HTTP.get(`${resource}/jugadores`);
    return response.data;
  },

  async findOne(id) {
    await sleep(1000);
    return (await HTTP.get(`${resource}/${id}`)).data;
  },

  async findByLogin(login) {
    await sleep(1000);
    return (await HTTP.get(`${resource}/perfil?login=${login}`)).data;
  },

  async findAllVentas(id) {
    await sleep(1000);
    return (await HTTP.get(`${resource}/venta/${id}`)).data;
  },

  async findAllCompras(id) {
    await sleep(1000);
    return (await HTTP.get(`${resource}/compra/${id}`)).data;
  },

  async findAllMeGusta(id) {
    await sleep(1000);
    return (await HTTP.get(`${resource}/megusta/${id}`)).data;
  },

  async findChats(id){
    const response = await HTTP.get(`${resource}/${id}/chats`);
    return response.data;
  },

  async delete(id) {
    await sleep(1000);
    return await HTTP.delete(`${resource}/${id}`);
  },

  async save(user) {
    if (user.id) {
      return (await HTTP.put(`${resource}/${user.id}`, user)).data;
    } else {
      return (await HTTP.post(`${resource}`, user)).data;
    }
  },
};

