import Vue from "vue";
import VueRouter from "vue-router";
import auth from "@/common/auth";
import store from "@/common/store";
import { NotFound, LoginWeb, InicioWeb, Home, Reservas} from "@/components";
import clubRouter from "@/entities/club/club.router.js";
import temaRouter from "@/entities/tema/tema.router.js";
import userRouter from "@/entities/usuario/usuario.router.js";
import pistaRouter from "@/entities/pista/pista.router.js";
import partidoRouter from "@/entities/partido/partido.router.js";
import bonoRouter from "@/entities/bono/bono.router.js";

const user = store.state.user;

Vue.use(VueRouter);

const routes = [
  {
    path: "/login-web",
    name: "LoginWeb",
    component: LoginWeb,
    meta: { public: true, isLoginPage: true },
  },
  {
    path: "/",
    name: "Home",
    component: Home,
    meta: { public: true },
  },
  {
    path: "/inicio-web",
    name: "InicioWeb",
    component: InicioWeb,
    meta: {public: false, authority: ['ADMIN_WEB', 'ADMIN_CLUB']}
  },
  {
    path: "/reservas",
    name: "Reservas",
    component: Reservas,
    meta: {public: false, authority: 'ADMIN_CLUB'}
  },
  { path: "*", component: NotFound, meta: { public: true } },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes: routes.concat(clubRouter).concat(temaRouter).concat(userRouter).concat(pistaRouter).concat(partidoRouter).concat(bonoRouter),
});

router.beforeEach((to, from, next) => {
  // Lo primero que hacemos antes de cargar ninguna ruta es comprobar si
  // el usuario está autenticado (revisando el token)
  auth.isAuthenticationChecked.finally(() => {
    // por defecto, el usuario debe estar autenticado para acceder a las rutas
    const requiresAuth = !to.meta.public;

    const requiredAuthority = to.meta.authority;
    const userIsLogged = user.logged;
    const loggedUserAuthority = user.authority;
    
    if (requiresAuth) {
      // página privada
      if (userIsLogged) {
        if (requiredAuthority && !requiredAuthority.includes(loggedUserAuthority)) {
          // usuario logueado pero sin permisos suficientes, le redirigimos a la página de login
          Vue.notify({
            text: "Acceso no permitido para este usuario. Intentelo de nuevo.",
            type: "error",
          });
          auth.logout();
          next("/");
        } else {
          // usuario logueado y con permisos adecuados
          next();
        }
      } else {
        // usuario no está logueado, no puede acceder a la página
        Vue.notify({
          text: "Esta pagina requiere autenticacion.",
          type: "error",
        });
        next("/");
      }
    } else {
      // página pública
      if (userIsLogged && to.meta.isLoginPage) {
        // si estamos logueados no hace falta volver a mostrar el login
          next({ name: "InicioWeb", replace: true });
      } else {
        next();
      }
    }
  });
});

export default router;
